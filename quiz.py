from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b''.join([
    b'gAAAAABczwGwZgDacU6c6DfgSKQOerd9r2ESKlqCNe_oRKE7pSBI_HLaszvwx',
    b'Aa__mjwHZAdA7b65-iz-FMdv073QC87w_3CqiKCyQmIEYbmru1wdycBTq97nz',
    b'UGoyzPL739TjHZvtJoakiINCiWpvNnQwR0eJd-XygkXApxOr2QyX3Xb9Tax6w='
    ])


def main():
    f = Fernet(key)
    print(f.decrypt(message))


main()
if __name__ != "__main__":
    main()
